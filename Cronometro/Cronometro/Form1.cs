﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cronometro
{
    public partial class Form1 : Form
    {
        Cronometro c;
        public Form1()
        {
            InitializeComponent();
            c = new Cronometro();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                lbl.Text = c.start().ToString();
        }
        private void Stop_Click(object sender, EventArgs e)
        {
           lbl.Text = c.stop().ToString();
        }
        private void btnreset_Click(object sender, EventArgs e)
        {
            lbl.Text = c.reset().ToString();
        }
    }
}
