﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cronometro
{
    class Cronometro
    {
        private DateTime _agora;
        private DateTime _stop;
        private TimeSpan _total;
        private bool click = false;
        private bool stoped = true;

        public DateTime Agora
        {
            get { return _agora; }
            set { _agora = value; }
        }

        public DateTime Stop
        {
            get { return _stop; }
            set { _stop = value; }
        }

        public TimeSpan Total
        {
            get { return _total; }
            set { _total = value; }
        }

        public String start()
        {
            stoped = true;
            if (!click)
            {
                click = true;
                Agora = DateTime.Now;
                return "0:0:0";
              
            }
            else
            {
                return Convert.ToString(Total.Hours) + ":" + Convert.ToString(Total.Minutes) + ":" + Convert.ToString(Total.Seconds);
            }
        }
        public String stop()
        {
            if (stoped)
            {
                Stop = DateTime.Now;
                Total = Stop.Subtract(Agora);
                stoped = false;
            }
            else
            {
                Total = Stop.Subtract(Agora);
            }
            return Convert.ToString(Total.Hours) + ":" + Convert.ToString(Total.Minutes) + ":" + Convert.ToString(Total.Seconds);
        }

        public String reset()
        {
            click = !true;
            return "0:0:0";
        }
    }
}
